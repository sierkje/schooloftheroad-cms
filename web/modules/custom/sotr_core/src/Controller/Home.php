<?php

namespace Drupal\sotr_core\Controller;

use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Controller routines for custom, default School of the Road routes.
 */
class Home {

  /**
   * Returns forum index page.
   *
   * @return array
   *   A render array.
   */
  public function home() {
    $build = [
      '#title' => new TranslatableMarkup('School of the Road'),
      '#markup' => '<p>This will be the homepage.</p>',
    ];
    return $build;
  }

}
