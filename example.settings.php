<?php

// @codingStandardsIgnoreFile

/**
 * @file
 * Example include for the site-specific configuration file.
 *
 * Add this code at the end of ./web/sites/default/settings.php to take effect.
 */

$databases = [
  'default' => [
    'default' => [
      'database' => getenv('SOTR_DB_NAME'),
      'username' => getenv('SOTR_DB_USER'),
      'password' => getenv('SOTR_DB_PW'),
      'host' => getenv('SOTR_DB_HOST'),
      'port' => getenv('SOTR_DB_PORT'),
      'driver' => 'mysql',
      'prefix' => '',
      'collation' => 'utf8mb4_general_ci',
    ],
  ],
];

$settings['hash_salt'] = getenv('SOTR_HASH_SALT');

$settings['file_public_base_url'] = getenv('SOTR_PUBLIC_FILES_URL');

$settings['file_public_path'] = getenv('SOTR_PUBLIC_FILES_PATH');

$settings['file_private_path'] = getenv('SOTR_PRIVATE_FILES_PATH');

#
# if (file_exists($app_root . '/' . $site_path . '/settings.local.php')) {
#   include $app_root . '/' . $site_path . '/settings.local.php';
# }
